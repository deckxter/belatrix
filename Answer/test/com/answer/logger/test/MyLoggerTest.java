package com.answer.logger.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.answer.logger.ConsoleLogger;
import com.answer.logger.DatabaseLogger;
import com.answer.logger.factory.ConsoleLoggerFactory;
import com.answer.logger.factory.DatabaseLoggerFactory;
import com.answer.message.Message;
import com.answer.message.TypeMessage;

public class MyLoggerTest {

	@Test
	public void testBadConfigurarionConsoleLogger() throws Exception {
		ConsoleLoggerFactory consoleLoggerFactory = new ConsoleLoggerFactory();
		ConsoleLogger consoleLogger = (ConsoleLogger) consoleLoggerFactory.getMyLogger(TypeMessage.ERROR.getLevel());

		Message message = new Message();
		message.setMessage("Bad Configuration of logger, nothing to log");
		message.setTypeMessage(TypeMessage.MESSAGE);
		
		Assert.assertNull(consoleLogger.printMessage(message));
	}
	
	@Test
	public void testOkConfigurationConsoleLogger() throws Exception {
		ConsoleLoggerFactory consoleLoggerFactory = new ConsoleLoggerFactory();
		ConsoleLogger consoleLogger = (ConsoleLogger) consoleLoggerFactory.getMyLogger(TypeMessage.MESSAGE.getLevel());

		Message message = new Message();
		message.setMessage("Logging...");
		message.setTypeMessage(TypeMessage.ERROR);
		
		Assert.assertNotNull(consoleLogger.printMessage(message));
	}
	
	@Test
	public void testOkConfigurationDatabaseLogger() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userName", "root");
		params.put("password", "Belatrix2018_");
		params.put("dbms", "mysql");
		params.put("serverName", "localhost");
		params.put("portNumber", "3306");
		params.put("dbName", "mysql");
		
		DatabaseLoggerFactory databaseLoggerFactory = new DatabaseLoggerFactory(params);
		DatabaseLogger databaseLogger = (DatabaseLogger) databaseLoggerFactory.getMyLogger(TypeMessage.WARNING.getLevel());
		
		Message message = new Message();
		message.setMessage("Log to database");
		message.setTypeMessage(TypeMessage.ERROR);
		
		Assert.assertNotNull(databaseLogger.printMessage(message));
	}
	
	@Test
	public void testBadConfigurationDatabaseLogger() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userName", "root");
		params.put("password", "Belatrix2018_");
		params.put("dbms", "mysql");
		params.put("serverName", "localhost");
		params.put("portNumber", "3306");
		params.put("dbName", "mysql");
		
		DatabaseLoggerFactory databaseLoggerFactory = new DatabaseLoggerFactory(params);
		DatabaseLogger databaseLogger = (DatabaseLogger) databaseLoggerFactory.getMyLogger(TypeMessage.WARNING.getLevel());
		
		Message message = new Message();
		message.setMessage("Log _to database");
		message.setTypeMessage(TypeMessage.MESSAGE);
		
		Assert.assertNull(databaseLogger.printMessage(message));
	}
}
