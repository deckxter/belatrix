package com.answer.logger;

import java.util.logging.Logger;

import com.answer.message.Message;

public abstract class MyLogger {
	private Logger logger;
	private Message message;
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	/**
	 * Logic to determine when execute logging and which logging
	 * @param message
	 * @return message to print
	 */
	protected String log(Message message) {
		if(message.getTypeMessage().getLevel() >= logger.getLevel().intValue()) {
			return message.toString();
		}
		return null;
	}
	
	public abstract Object printMessage(Message message) throws Exception;
	
}
