package com.answer.logger.factory;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

import com.answer.logger.ConsoleLogger;
import com.answer.logger.MyLogger;

public class ConsoleLoggerFactory extends LoggerAbstractFactory {
	private static ConsoleHandler consoleHandler;
	
	public ConsoleLoggerFactory() {
		consoleHandler = new ConsoleHandler();
	}
	
	public MyLogger getMyLogger(int level) throws Exception {
		Logger logger = LoggerAbstractFactory.getLogger(level);
		logger.addHandler(consoleHandler);
		
		ConsoleLogger consoleLogger = new ConsoleLogger();
		consoleLogger.setLogger(logger);
		
		return consoleLogger;
	}
}
