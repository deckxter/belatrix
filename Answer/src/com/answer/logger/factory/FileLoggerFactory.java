package com.answer.logger.factory;

import java.io.File;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.answer.logger.FileLogger;
import com.answer.logger.MyLogger;

public class FileLoggerFactory extends LoggerAbstractFactory {
	private File logFile;
	private FileHandler fileHandler;
	
	public FileLoggerFactory(Map<?, ?> dbParams) throws Exception {
		logFile = new File(dbParams.get("logFileFolder") + "/logFile.txt");
		
		if (!logFile.exists()) {
			logFile.createNewFile();
		}
		
		fileHandler = new FileHandler(dbParams.get("logFileFolder") + "/logFile.txt");
	}
	
	public MyLogger getMyLogger(int level) throws Exception {
		Logger logger = super.getLogger(level);
		logger.addHandler(fileHandler);
		
		FileLogger fileLogger = new FileLogger();
		fileLogger.setLogger(logger);
		
		return fileLogger;
	}
}
