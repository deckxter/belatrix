package com.answer.logger.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.answer.logger.DatabaseLogger;
import com.answer.logger.MyLogger;

public class DatabaseLoggerFactory extends LoggerAbstractFactory {
	private Connection connection;
	private Properties connectionProps;
	
	public DatabaseLoggerFactory(Map<?, ?> dbParams) throws Exception {
		connection = null;
		connectionProps = new Properties();
		connectionProps.put("user", dbParams.get("userName"));
		connectionProps.put("password", dbParams.get("password"));

		connection = DriverManager.getConnection("jdbc:" + dbParams.get("dbms") + "://" + dbParams.get("serverName")
				+ ":" + dbParams.get("portNumber") + "/" +dbParams.get("dbName"), connectionProps);
	}

	public MyLogger getMyLogger(int level) throws Exception {
		Logger logger = super.getLogger(level);
		
		Statement statement = connection.createStatement();
		
		DatabaseLogger databaseLogger = new DatabaseLogger();
		databaseLogger.setStatement(statement);
		databaseLogger.setLogger(logger);
		
		return databaseLogger;
	}

}
