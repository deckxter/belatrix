package com.answer.logger.factory;

import com.answer.logger.MyLogger;

public class LoggerFactory {
	public static MyLogger getMyLogger(LoggerAbstractFactory factory, int level) throws Exception {
		return factory.getMyLogger(level);
	}
}
