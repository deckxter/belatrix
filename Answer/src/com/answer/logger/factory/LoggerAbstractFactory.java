package com.answer.logger.factory;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.answer.logger.MyLogger;

public abstract class LoggerAbstractFactory {
	protected static Logger getLogger(int level) throws Exception {
		
		Logger logger = Logger.getLogger("MyLogger");
		
		switch(level) {
			case 800:
				logger.setLevel(Level.INFO);
				break;
			case 900:
				logger.setLevel(Level.WARNING);
				break;
			case 1000:
				logger.setLevel(Level.SEVERE);
				break;
			default :
				throw new Exception("Must be specified a valid log level");
		}
		
		return logger;
	}
	
	public abstract MyLogger getMyLogger(int level) throws Exception;
}
