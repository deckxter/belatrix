package com.answer.logger;

import java.sql.Statement;

import org.apache.commons.lang3.StringUtils;

import com.answer.message.Message;

public class DatabaseLogger extends MyLogger {
	private Statement statement;

	public Statement getStatement() {
		return statement;
	}

	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	public Integer printMessage(Message message) throws Exception {
		String printMsg = super.log(message);
		
		//TODO Manage Connections try, catch, pool connection, something...
		if(StringUtils.isNotBlank(printMsg)) {
			String query = "insert into Log_Values(log) value ('" + printMsg  + "')";
			return statement.executeUpdate(query);
		}
		return null;
	}
	
}
