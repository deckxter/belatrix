package com.answer.logger;

import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.answer.message.Message;

public class ConsoleLogger extends MyLogger {

	@Override
	public String printMessage(Message message) {
		String printMsg = super.log(message);
		
		if(StringUtils.isNotBlank(printMsg)) {
			Logger logger = this.getLogger();
			logger.log(logger.getLevel(), printMsg);
		}
		
		return printMsg;
	}

}
