package com.answer.message;

public enum TypeMessage {
	//Info
	MESSAGE("Message ", 800, "INFO"),
	
	//Warning
	WARNING("Warning ", 900, "WARNING"),
	
	//Severe
	ERROR("Error ", 1000, "SEVERE");
	
	private String type;
	private int level;
	private String loggerType;
	
	TypeMessage(String type, int level, String loggerType) {
		this.type = type;
		this.level = level;
		this.loggerType = loggerType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getLoggerType() {
		return loggerType;
	}

	public void setLoggerType(String loggerType) {
		this.loggerType = loggerType;
	}

}
