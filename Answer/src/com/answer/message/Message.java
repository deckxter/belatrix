package com.answer.message;

import java.text.DateFormat;
import java.util.Date;

public class Message {
	private String message;
	private TypeMessage typeMessage;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TypeMessage getTypeMessage() {
		return typeMessage;
	}

	public void setTypeMessage(TypeMessage typeMessage) {
		this.typeMessage = typeMessage;
	}

	@Override
	public String toString() {
		return typeMessage.getType() + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) +" " + message;
	}
}
